var fs = require('fs');
var Q = require('q');
var curl = require('node-curl')
var page_list = require('./pagelist')
var CONFIG = require('./config.js');


function getContent(urlSlug) {
    var deferred = Q.defer();
    var _url = combineUrl(urlSlug);
    curl(_url, function(err) {
        if (err) {
            console.log(err);
            deferred.reject(new Error(err));
        } else {
            deferred.resolve({
                filename: urlSlug,
                content: this.body
            });
        }
    })
    return deferred.promise;
}


function saveFile(fileData) {
    var deferred = Q.defer();
    //var path = '../modernweb2015/';
    var path = CONFIG.pathToSave;
    var _filename = path + makeFilename(fileData.filename);
    var _content = fileData.content;
    fs.writeFile(_filename, _content, {
        encoding: 'utf8'
    }, function(err) {
        if (err) {
            console.log(err);
            deferred.reject(new Error(err));
        } else {
            deferred.resolve({
                savedFilename: _filename
            });
        }
    })
    return deferred.promise;
}

function combineUrl(slug) {
    var host = CONFIG.hostPath;
    if (slug == "/") {
        return host;
    } else {
        return host + slug + "/";
    }
}

function makeFilename(slug) {
    if (slug == "/") {
        return "index.html";
    } else {
        return slug + ".html";
    }
}

function filterContent(fileData) {
    var deferred = Q.defer();
    var _content = fileData.content;
    //移除robots指示
    _content = _content.replace(/<meta name='robots' content='noindex,follow' \/>\s*\n/g, "");
    //移除一些wp的meta
    _content = _content.replace(/<meta name="generator" content="WordPress 4.1.1" \/>\s*\n/g, "");
    _content = _content.replace(/.*EditURI.*\n/, "");
    _content = _content.replace(/.*wmanifest.*\n/, "");
    _content = _content.replace(/.*alternate.*\n/, "");
    _content = _content.replace(/.*canonical.*\n/, "");
    _content = _content.replace(/.*shortlink.*\n/, "");
    //相對路徑化
    _content = _content.replace(/http:\/\/estellewang\.com\/website\/case\/modern-web\/wp-content\//g, "");
    _content = _content.replace(/http:\/\/estellewang\.com\/website\/case\/modern-web\/wp-includes\//g, "");
    _content = _content.replace(/http:\/\/estellewang\.com\/website\/case\/modern-web/g, "/");
    //將path改成靜態檔名
    _content = _content.replace(/href="\/\/(.*?)\/"/g, "href=\"$1\.html\"");
    fileData.content = _content;
    deferred.resolve(fileData);
    return deferred.promise;
}


function crawl(urlArray) {
    var targetUrlSlug = urlArray.shift();
    getContent(targetUrlSlug)
        .then(filterContent)
        .then(saveFile)
        .then(function(result) {
            console.log(result.savedFilename + " is saved");
            if (urlArray.length > 0) crawl(urlArray);
        })
}

// start crawl page and save files
crawl(page_list);
