# Parse WP page

## 安裝
1. npm install
1. node grap-pages.js

# 抓取轉換檔案的列表 - pagelist.json
需要轉檔的網頁名稱列在pagelist.json中，如果有需要加入新的頁面，只需要加入新的檔名即可。

# 設定檔 - config.js
  - 設定檔案儲存路徑
  - 設定抓取網站的基本網址

ps. Ubuntu安裝node-curl如有問題，可能是缺少編譯套件造成，可先安裝```sudo apt-get install libcurl3-dev```
